# LifeBit
Welcome to LifeBit a twitch.tv game that is played using bits!

# Set-up
Set-up is simple visit https://lifebit.herokuapp.com fill out the config page and hit submit to get your url
- Bot able to talk? - (requires an O-Auth token) set this on to let LifeBit talk with your chat.
- Allow Overshell? - Give the current defender a chance to defend themselves by paying bits Caps at 250bits
- Overshell Color - Don't like the shields color change it.
- Life Color/Damage Color - don't like the color of the bar itself these two settings let you change those
- Bot Name - Specify the name of your bot you use for twitch
- Twitch Channel - Your Twitch channel
- Bot OAuth - only needed if you turn talking on does not need the preceeding OAuth: 
- config name - give it something unique.

# Really have to run it locally?
This app is running Sinatra on Ruby 2.6.2.

1. clone this repo
2. `bundle install`
3. `./app.rb`