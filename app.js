const express = require('express');
const path = require('path');
const sass = require('node-sass');
const sassMiddleware = require('node-sass-middleware');
const app = express();
const port = 4567;


app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'slm');

app.use(sassMiddleware({
    /* Options */
    src: path.join(__dirname, 'views'),
    dest: path.join(__dirname, 'public'),
    debug: true,
    outputStyle: 'compressed',
    prefix:  '/'  // Where prefix is at <link rel="stylesheets" href="prefix/style.css"/>
}));


sass.render({
  file: './application.scss'
});

sass.render({
  file: './life.scss'
});


app.get('/', function (req, res) {
  res.render('layout');
  // res.render('index');
});

app.get('/life', function (req, res){
  res.render('life');
});

app.get('/application.js', function(req, res){
  res.sendFile(path.join(__dirname, 'views/application.js'));
});

app.get('/lifebit.js', function(req, res){
  res.sendFile(path.join(__dirname, 'views/lifebit.js'));
});

app.get('/lifebar.css', function(req, res){
  res.sendFile(path.join(__dirname, 'public/lifebar.css'));
});

app.get('/demo.css', function(req, res){
  res.sendFile(path.join(__dirname, 'public/demo.css'));
});

app.get('/application.css', function(req, res){
  res.sendFile(path.join(__dirname, 'public/application.css'));
});

app.get('/images/lifebit.png', function(req, res){
  res.sendFile(path.join(__dirname, 'public/images/lifebit.png'));
});


app.listen(port, () => console.log(`LifeBit listening on port ${port}!`));