/* global $ */

$(document).ready(function () {
  // Getting vars
  let scolor = $("#scolor").val(),
      lcolor = $("#lcolor").val(),
      dcolor = $("#dcolor").val(),
      tcolor = $("#tcolor").val();
  // Set initial Values of Demobar
  $("#demobar_shell").css("box-shadow", "inset 0 0 10px 5px " + scolor);
  $("#demobar_health").css("background-color", lcolor);
  $("#demobar_container").css("background-color", dcolor);
  $("#demobar_status").css("color", tcolor);
  $("#demobar_name").css("color", tcolor);
  //Hide the Demobar's shell
  $("#scolor_lbl").hide();
  $("#scolor").hide();
  $("#isshell").hide();
  $("#demobar_shell").hide();
  $("#oshell").change(function () {
    if (this.checked) {
      $("#scolor_lbl").show();
      $("#scolor").show();
      $("#demobar_shell").show();
      $("#isshell").show();
      $("#scolor").change(function(){
        var scolor = $(this).val();
        $("#demobar_shell").css("box-shadow", "inset 0 0 10px 5px " + scolor);
      });
    } else {
      $("#scolor_lbl").hide();
      $("#scolor").hide();
      $("#isshell").hide();
      $("#demobar_shell").hide();
      $("#demobar_shell").css("box-shadow", "");
    }
  });
  $("#lcolor").change(function(){
    var lcolor = $(this).val();
    $("#demobar_health").css("background-color", lcolor);
  });
  $("#dcolor").change(function(){
    var dcolor = $(this).val();
    $("#demobar_container").css("background-color", dcolor);
  });

  $("#tcolor").change(function(){
    var tcolor = $(this).val();
    $("#demobar_status").css("color", tcolor);
    $("#demobar_name").css("color", tcolor);
  });

});

function toggleDarkLight() {
  var body = document.getElementById("body");
  var currentClass = body.className;
  body.className = currentClass === "dark-mode" ? "light-mode" : "dark-mode";
}




