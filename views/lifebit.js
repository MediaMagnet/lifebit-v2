/* global hpCurrent defaultHealth defaultShell healthTotal shellTotal hpMax
shield oShell iName lifebar_name lifebar_health lifebar_container lifebar_status
iOauth talks localStorage iChan*/

class LifeBit {

  constructor(name, health, healthMax, shield) {
    this.defaultName = iName;
    this.defaultHealth = 100;
    this.defaultShield = 50;
    this.defaultShieldMax = 250;
    this.debug = false;
    this.talks = talks;
    this.tmrId = -1;
    this.setDefenderName(name || this.defaultName);
    this.setDefenderHealth(health || this.defaultHealth);
    this.setDefenderHealthMax(healthMax || this.defaultHealth);
    this.setDefenderShield(shield || this.defaultShield);

    this.UpdateHealthTrigger();
  }

  getDefaultDefenderName() {
    return this.defaultName;
  }

  getDefenderName() {
    return localStorage.getItem("defName");
  }

  setDefenderName(name) {
    if (localStorage.getItem("defName") !== name) {
      localStorage.setItem("defName", name);
    }
    this.showNewDefender();
    return name;
  }

  getDefaultDefenderHealth() {
    return this.defaultHealth;
  }

  getDefenderHealth() {
    return parseInt(localStorage.getItem("curHealth"));
  }

  setDefenderHealth(health) {
    if (localStorage.getItem("curHealth") !== health) {
      localStorage.setItem("curHealth", health);
    }
    this.UpdateHealthTrigger();
    return parseInt(health);
  }

  getDefaultDefenderHealthMax() {
    return this.defaultHealth;
  }

  getDefenderHealthMax() {
    return parseInt(localStorage.getItem("curHealthMax"));
  }

  setDefenderHealthMax(health) {
    if (localStorage.getItem("curHealthMax") !== health) {
      localStorage.setItem("curHealthMax", health);
    }
    this.UpdateHealthTrigger();
    return parseInt(health);
  }

  getDefaultDefenderShield() {
    return this.defaultShield;
  }

  getDefenderShield() {
    return parseInt(localStorage.getItem("curShield"));
  }

  setDefenderShield(shield) {
    if (shield > this.defaultShieldMax) {
      shield = this.defaultShieldMax;
    }
    if (localStorage.getItem("curShield") !== shield) {
      localStorage.setItem("curShield", shield);
    }
    this.UpdateHealthTrigger();
    return parseInt(shield);
  }

  // Damages the Defender and returns whether or not the shield is depleted.
  damageDefender(damage) {
    if (oShell === true && this.getDefenderShield() > 0) { //for Overshell
      this.setDefenderShield(this.getDefenderShield() - damage);
      if (this.getDefenderShield() <= 0) {
        damage = Math.abs(this.getDefenderShield());
        this.setDefenderShield(0);
        this.setDefenderHealth(this.getDefenderHealth() - damage);
      }
    } else { //for lifebar only
      this.setDefenderHealth(this.getDefenderHealth() - damage);
    }
    return oShell && this.getDefenderShield() <= 0;
  }

  showNewDefender() {
    lifebar_name.innerHTML = this.getDefenderName();
    this.UpdateHealthTrigger();
  }

  UpdateHealthTrigger() {
    let hpCurrent = this.getDefenderHealth();
    let hpMax = this.getDefenderHealthMax();
    let shield = this.getDefenderShield();

    if (this.debug) {
      console.log(hpCurrent, hpMax, shield);
    }
    clearTimeout(this.tmrId);
    this.tmrId = setTimeout(this.UpdateHealth.bind(this), 500);
  }

  UpdateHealth() {
    let disp = `${this.getDefenderHealth()} / ${this.getDefenderHealthMax()}`;
    lifebar_health.style['width'] = (this.getDefenderHealth() / this.getDefenderHealthMax() * 100.0) + '%';

    if (oShell === true && this.getDefenderShield() > 0) {
      disp += `<span>(${this.getDefenderShield()})</span>`;
      lifebar_container.classList.add('shell');
    } else if (oShell === false || this.getDefenderShield() === 0) {
      lifebar_container.classList.remove('shell');
    }

    lifebar_status.innerHTML = disp;
  }
}

// Could probably do better checking for this using browsertype
if (typeof (window)["obsstudio"] != "undefined") {
  document.querySelector("body").style.background = "transparent";
}

lifebar_container.style.backgroundColor = dColor;
lifebar_shell.style.boxShadow = 'inset 0 0 10px 5px ' + sColor;
lifebar_health.style.backgroundColor = lColor;
lifebar_status.style.color = tColor;
lifebar_name.style.color = tColor;

const ws = new WebSocket('wss://irc-ws.chat.twitch.tv');
const lifebit = new LifeBit(localStorage.getItem("defName"), localStorage.getItem("curHealth"), localStorage.getItem("curHealthMax"), localStorage.getItem("curShield"));

ws.onmessage = function (chatMsg) {
  let lines = chatMsg.data.split('\r\n');
  for (let l = 0; l < lines.length; l++) {
    let line = lines[l].trim();
    if (line === '')
      continue;

    let lineParts = line.split(' ');

    if (lifebit.debug)
      console.log(line);

    if (lineParts[0] === 'PING') {
      ws.send('PONG ' + line.split(':')[1]);
    } else if (lineParts[2] === 'PRIVMSG') {
      let parts = line.split('PRIVMSG'); //everything after here is user text
      let metadata = parts[0].split(';'); //metadata from twitch
      let user = metadata[metadata.length - 1].split(':')[1].split('!')[0]; //username

      parts[1] = parts[1].trim();
      let cIdx = parts[1].indexOf(':');
      if (parts[1].indexOf('#chatrooms:') === 0) {
        cIdx = parts[1].indexOf(':'.cIdx + 15);
      }

      let msg = parts[1].substring(cIdx + 1);

      for (var i = 0; i < metadata.length; i++) {

        if (metadata[i].indexOf('badges=') === 0) {

          if (metadata[i].split('=')[1].indexOf('broadcaster') > -1 || metadata[i].split('=')[1].indexOf('moderator') > -1) {

            switch (msg.split(" ")[0]) {

              case '!sethealth': //admin force health value
                let newHP = msg.split(" ")[1];
                lifebit.setDefenderHealth(newHP);
                break;

              case '!setmax': //admin force max health value
                let newMax = msg.split(" ")[1];
                lifebit.setDefenderHealthMax(newMax);
                break;

              case '!setshell': //admin force shell value
                let newShell = msg.split(" ")[1];
                lifebit.setDefenderShield(newShell);
                break;

              case '!damage': //admin hurt the defender
                let dmgVal = msg.split(" ")[1];
                if (lifebit.damageDefender(dmgVal)) {
                  lifebar_container.classList.remove('shell');
                }
                break;

              case '!setname':
                let newDefender = msg.split(" ")[1];
                lifebit.setDefenderName(newDefender);
                break;

              case '!talktest':
                console.log(iChan);
                if (lifebit.talks === true) {
                  console.log('testing talk');
                  console.log('PRIVMSG #' + iChan + " :I've got a lovely bunch of coconuts");
                  ws.send('PRIVMSG #' + iChan + " :I've got a lovely bunch of coconuts");
                  ws.send('PRIVMSG #' + iChan + " :There they are all standing in a row");
                }
                break;

              default:
                break;
            }
          }
        }

        if (metadata[i].indexOf('bits=') === 0) {

          let bitCount = parseInt(metadata[i].split('=')[1], 10);
          // some checks first
          if (user === lifebit.getDefenderName() && oShell === true) {
            lifebit.setDefenderShield(lifebit.getDefenderShield() + bitCount);
          } else {
            if (lifebit.damageDefender(bitCount)) {
              lifebar_container.classList.remove('shell');
            }
          }
        }

        // Check if New Defender has risen
        if (lifebit.getDefenderHealth() <= 0) {
          let lifeVal = Math.abs(lifebit.getDefenderHealth()) > lifebit.getDefaultDefenderHealth() ? Math.abs(lifebit.getDefenderHealth()) : lifebit.getDefaultDefenderHealth() ; // |current| less than 100? yes use default no use |current|
          let newDefender = user;
          lifebit.setDefenderHealth(lifeVal);
          lifebit.setDefenderHealthMax(lifeVal);
          lifebit.setDefenderName(newDefender);
          if(lifebit.talks === true){
            ws.send('PRIVMSG #' + iChan + ' :A new defender, ' + newDefender + ', takes the stage with ' +
            lifeVal + ' health! Good luck!');
          }
        }
      }
    }
  }
};

console.log(talks);
//Opens Web Socket for Twitch
ws.onopen = function open(e) {
  if (lifebit.debug)
    console.log('WS open, hello twitch.', e);
  this.send('CAP REQ :twitch.tv/tags twitch.tv/commands twitch.tv/membership');

  if (lifebit.talks) {
    this.send('PASS oauth:' + iOauth);
    this.send('NICK ' + iName);
  } else {
    this.send('NICK justinfan' + Math.floor(Math.random() * 999999));
  }
  this.send('JOIN #' + iChan)
};
